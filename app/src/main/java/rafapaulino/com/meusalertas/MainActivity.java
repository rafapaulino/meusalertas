package rafapaulino.com.meusalertas;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clicaTela(View view) {

        Log.i("LogX","Clicou na tela");

        //toast
//      Toast meuToast;
//      meuToast = Toast.makeText(MainActivity.this,"Eu sou o Toast!", Toast.LENGTH_SHORT);
//      meuToast.show();

        Toast.makeText(getApplicationContext(),"Eu sou o Toast!", Toast.LENGTH_SHORT).show();

        //snackbar
        Snackbar meuSnack;
        meuSnack = Snackbar.make(view, "Meu Snack!", Snackbar.LENGTH_SHORT);
        meuSnack.show();

        meuSnack.setAction("Clique aqui infeliz!", new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Log.i("LogX","Clicou no snack");
            }
        });

        //alerta
        AlertDialog.Builder construtorAlerta;
        construtorAlerta = new AlertDialog.Builder(this);
        construtorAlerta.setTitle("Título do Alerta");
        construtorAlerta.setMessage("Qual é a sua opção?");
        construtorAlerta.setIcon(R.drawable.imagem01);

        construtorAlerta.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.i("LogX","Clicou no SIM!!!");
            }
        });

        construtorAlerta.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.i("LogX","Clicou no NÃO!!!");
            }
        });

        construtorAlerta.setNeutralButton("Neutro", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.i("LogX","Clicou no NEUTRO!!!");
            }
        });

        AlertDialog meuAlerta = construtorAlerta.create();
        meuAlerta.show();

    }

}
